import { Product } from '../product/Product';
import { ProductCompatibilityStatus } from '../productCompatibilityStatus/ProductCompatibilityStatus';

export class Bundle {
	public ProductList: Product[];
    public CompatibilityList: ProductCompatibilityStatus[];    
}