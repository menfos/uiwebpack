﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'adminHome.component.html',
    styleUrls: ['adminHome.component.css']
})
//this is layout page for adminHome with left menu
export class AdminHomeComponent {
}