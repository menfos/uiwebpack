import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RequestService {
    public _baseUrl = 'http://192.168.225.192/';
    public appComponent: any = null;

    constructor( private http: Http) {
    }
    
    get(url: String) {
        if(this.isAuthenticated())
        {
            let headers = new Headers({ 'authorization': 'Bearer ' + localStorage.getItem('token') });
            let options = new RequestOptions({ headers: headers });

            return this.http.get(this._baseUrl + url,options).catch(this.handleError);
        }
        else    
        {
            this.appComponent.signOut();

            return new Observable<Response>();
        }
    }

    post(url: String, obj: any) {
        if(this.isAuthenticated())
        {
            let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
            let options = new RequestOptions({ headers: headers });

            return this.http.post(this._baseUrl + url,JSON.stringify(obj), options).catch(this.handleError);
        }
        else  
        {
            this.appComponent.signOut();

            return new Observable<Response>();
        }
    }

    put(url: String, obj: any) {
        if(this.isAuthenticated())
        {
            let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
            let options = new RequestOptions({ headers: headers });
        
            return this.http.put(this._baseUrl + url, JSON.stringify(obj), options).catch(this.handleError);
        }
        else 
        { 
            this.appComponent.signOut();

            return new Observable<Response>();
        }
    }

    delete(url: String) {
        if(this.isAuthenticated())
        {
            let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token')});
            let options = new RequestOptions({ headers: headers });

            return this.http.delete(this._baseUrl + url, options).catch(this.handleError);
        }
        else
        {  
            this.appComponent.signOut();

            return new Observable<Response>();
        }
    }

    private handleError(error: Response) {
        alert(error.statusText + ", status - "+error.status);

        return Observable.throw('Server error');
    }

    private isAuthenticated(): boolean {
        const expiresAt = JSON.parse(localStorage.getItem('expires_at')); 

        return new Date().getTime() < expiresAt;         
    }
}