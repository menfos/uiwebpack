import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { AppComponent }   from './app.component';
import { RouterModule } from '@angular/router';
import { RequestService } from './shared/request.service';
import { AuthenticationService } from './shared/authentication.service';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProductAddComponent } from './components/product/administrator/productAdd.component';
import { AdminHomeComponent } from './components/administrator/adminHome.component';
import { AdminInfoComponent } from './components/administrator/adminInfo.component';
import { ProductComponentAdmin } from './components/product/administrator/products.component';
import { CompatibilityMatrixComponentAdmin } from './components/compatibilityMatrix/administrator/compatibilityMatrix.component';
import { ProductComponentUser } from './components/product/user/products.component';
import { CompatibilityMatrixComponentUser } from './components/compatibilityMatrix/user/compatibilityMatrix.component';
import { UserAddComponent } from './components/user/userAdd.component';
import { UserEditComponent } from './components/user/userEdit.component';
import { UserComponent } from './components/user/users.component';
import { UserHomeComponent } from './components/user/userHome.component';
import { UserInfoComponent } from './components/user/userInfo.component';
import { UserChangePasswordComponent } from './components/user/userChangePassword.component';

import { SharedServicesModule } from './shared/services/shared.services.module'
import { OAuthHandshakeModule } from './shared/login-callback/oauth-callback.module';
import { OAuthCallbackHandler } from './shared/login-callback/oauth-callback.guard';
import { OAuthCallbackComponent } from './shared/login-callback/oauth-callback.component';

@NgModule({
    imports:      [
        SharedServicesModule,
        OAuthHandshakeModule,
    	BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,        
        RouterModule.forRoot([
                { path: 'id_token', component: OAuthCallbackComponent, canActivate: [OAuthCallbackHandler] },
                { path: '', redirectTo: 'home', pathMatch: 'full'},
                { path: 'home', component: HomeComponent},
                { path: 'admin-home', component: AdminHomeComponent,
                    children: [
                        { path: '', redirectTo: 'info', pathMatch: 'full' },
                        { path: 'info', component: AdminInfoComponent },
                        { path: 'accounts', component: UserComponent }, 
                        { path: 'userAdd', component: UserAddComponent },
                        { path: 'userEdit/:user', component: UserEditComponent },
                        { path: 'compatibility', component: CompatibilityMatrixComponentAdmin },
                        { path: 'products', component: ProductComponentAdmin },
                        { path: 'productAdd/:country', component: ProductAddComponent },
                        { path: 'changePassword', component: UserChangePasswordComponent},
                        { path: '**', redirectTo: 'info', pathMatch: 'full' }
                         //todo: add other components
                    ]
                },
                {
                path: 'user-home', component: UserHomeComponent,
                    children: [
                        { path: '', redirectTo: 'info', pathMatch: 'full' },
                        { path: 'info', component: UserInfoComponent },
                        { path: 'products', component: ProductComponentUser },
                        { path: 'compatibility', component: CompatibilityMatrixComponentUser },
                        { path: 'changePassword', component: UserChangePasswordComponent},
                        { path: '**', redirectTo: 'info', pathMatch: 'full' }
                        //todo: add other components
                    ]
                },
                { path: 'login', component: LoginComponent },
                { path: '**', redirectTo: 'home', pathMatch: 'full' }
                //todo: add other components
        ], { useHash: true })],

    providers: [ AppComponent, AuthenticationService, RequestService],
    
    declarations: [ 
        HomeComponent, AppComponent, LoginComponent,
        ProductAddComponent, AdminInfoComponent, AdminHomeComponent, 
        CompatibilityMatrixComponentAdmin, ProductComponentAdmin,
        UserAddComponent, UserComponent, UserHomeComponent, UserInfoComponent, 
        ProductComponentUser, CompatibilityMatrixComponentUser,
        UserChangePasswordComponent, UserEditComponent
        ],

    bootstrap:    [ AppComponent ]
})

export class AppModule { }