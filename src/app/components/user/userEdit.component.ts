import { Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http,Response,Headers } from '@angular/http';
import { RequestService } from '../../shared/request.service';

@Component({
    moduleId: module.id,
    templateUrl: 'userEdit.component.html'
})
export class UserEditComponent {
    private user: any;
    private roles: string[];

    constructor(private rs: RequestService, http: Http, private router: Router,private activateRoute: ActivatedRoute) {
        this.user = {
            "Email" : '',
            "Username" :'',
            "FirstName" :'',
            "LastName" :'',
            "RoleName" :''
        };   
        this.roles = [];
        this.rs.get('api/roles')
            .toPromise()
            .then((response:Response) =>{
                let roleList = response.json();
                for(let index in roleList)
                {
                  let role = roleList[index];

                  this.roles.push(role.name);
                }
            })
            .catch((error:Error) => {
            });
        this.rs.get('api/accounts/user/'+activateRoute.snapshot.params['user'])
            .toPromise()
            .then((response:Response)=>{
                let model = response.json();
                let userData = model.fullName.split(" ");
                this.user.Email = model.email;
                this.user.Username = model.userName;
                this.user.RoleName = model.roles[0];
                this.user.FirstName = userData[0];
                this.user.LastName = userData[1];
                
            })
            .catch((error:Error) => {
            });

    }

    Done(myItem: any){
        if(myItem.RoleName == '')
            myItem.RoleName ='Admin';
        this.rs.put('api/accounts/user/edit/'+myItem.Username, myItem)
            .toPromise()
            .then(response => this.router.navigate(['../../accounts'], { relativeTo: this.activateRoute }))
            .catch((error:Error) => {
            });        
    }

}