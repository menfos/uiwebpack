import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
    constructor() {
    }
    public get AdalConfig(): any {
        return {
            tenant: 'cc8aa0e8-6ad4-4707-a4fe-f081ee998777',
            clientId: 'a4d0b31f-ce3d-4915-86c2-daa3d5ececca',
            resource: 'swda',
            redirectUri: window.location.origin + '/',
            postLogoutRedirectUri: window.location.origin + '/'
        };
    }
}