import { Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http,Response,Headers } from '@angular/http';
import { RequestService } from '../../shared/request.service';

@Component({
    moduleId: module.id,
    templateUrl: 'userAdd.component.html'
})
export class UserAddComponent {
    private user: any;
    private roles: string[];

    constructor(private rs: RequestService, http: Http, private router: Router,private activateRoute: ActivatedRoute) {
        this.roles = [];
        this.rs.get('api/roles')
            .toPromise()
            .then((response:Response) =>{
                let roleList = response.json();
                for(let index in roleList)
                {
                  let role = roleList[index];

                  this.roles.push(role.name);
                }
            })
            .catch((error:Error) => {
            });
        this.user = {
            "Email" : '',
            "Username" :'',
            "FirstName" :'',
            "LastName" :'',
            "RoleName" :'',
            "Password" :'',
            "ConfirmPassword" :''
        };       
    }

    Done(myItem: any){
        if(myItem.RoleName == '')
            myItem.RoleName ='Admin';
        if(myItem.Password == myItem.ConfirmPassword){
            this.rs.post('api/accounts/create', myItem)
                .toPromise()
                .then(response => this.router.navigate(['../accounts'], { relativeTo: this.activateRoute }))
                .catch((error:Error) => {
                });
        }else{
            alert("Passwords don't match.");
        }        
    }

}