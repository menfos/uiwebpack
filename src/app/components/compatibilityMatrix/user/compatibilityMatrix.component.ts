import { Component } from '@angular/core';
import { Http,Response,Headers } from '@angular/http';
import { ActivatedRoute, Router} from '@angular/router';
import { Product } from '../../../../app/components/product/Product';
import { ProductCompatibilityStatus } from '../../../../app/components/productCompatibilityStatus/ProductCompatibilityStatus';
import { Bundle } from '../../../../app/components/bundle/Bundle';
import { RequestService } from '../../../shared/request.service';
import 'rxjs/Rx';

@Component({
    moduleId: module.id,
    templateUrl: 'compatibilityMatrix.component.html',
    styleUrls: ['compatibilityMatrix.component.css']
}) 

export class CompatibilityMatrixComponentUser {
    private compatibilityMatrix: string[][];
    private bundle: Bundle;
    private country:string;
    private countries: string[];
    private changeList: ProductCompatibilityStatus[];
    private isSearch: boolean;
    private searchProductList: string[];
    private searchText: string;

    constructor(http: Http,private rs: RequestService, private router: Router, private activateRoute: ActivatedRoute) {
       this.bundle = new Bundle();
       this.changeList = this.searchProductList = [];
       this.country = this.searchText = '';
       this.isSearch = false;
       this.rs.get('api/countries')
              .toPromise()
              .then(response =>response.json())
              .then((result:any) => this.countries = result)
              .catch((error:Error) => {
            });
    }

    public setCountry(country: string){
      this.isSearch = false;
      this.searchText = '';
      this.country = country;
      this.refreshMatrix();
    }
    
    public refreshMatrix(){
      this.isSearch = false;
      this.searchText = '';
      this.rs.get('api/data/'+this.country)
              .toPromise()
              .then(response =>response.json())
              .then((result:any) =>this.bundle = result)
              .then(response => this.initializeCompatibilityMatrix(this.bundle.ProductList))
              .catch((error:Error) => {
            });
    }

    private initializeCompatibilityMatrix(productsList: Product[]){
       this.compatibilityMatrix = [];

       for (var row:number =0; row <= productsList.length; row++) 
       {
         this.compatibilityMatrix[row] = [];
       }

       this.compatibilityMatrix[0][0] = this.country;

       for(var row:number=1; row <= productsList.length; row++)
       {
             this.compatibilityMatrix[0][row] = productsList[row-1].ProductId;
             this.compatibilityMatrix[row][0] = productsList[row-1].ProductId;
       }

       for (var row:number = 1; row <= productsList.length; row++) 
       {  
         for (var column:number =1; column <= productsList.length; column++) 
         {
             this.compatibilityMatrix[row][column] = this.getCompatibleStatus(this.compatibilityMatrix[row][0], this.compatibilityMatrix[0][column]); 
         }
       }
    }

    private getCompatibleStatus(product1:string, product2:string):string{
      for (var index = 0; index < this.bundle.CompatibilityList.length; index++) 
      {

        if(this.bundle.CompatibilityList[index].ProductId == product1 && 
           this.bundle.CompatibilityList[index].CompatibleProductId == product2)
        {

          if(this.bundle.CompatibilityList[index].CompatibilityStatus)
          {
              return "1";
          }
          else
          {
              return "0";
          }
        }
      }
      return "";
    }

    private Search(productName: string){
      this.searchProductList = [];
        var count = 1;
        for (var row:number = 1; row < this.compatibilityMatrix[0].length; row++) 
        {
            if(this.compatibilityMatrix[row][0].includes(productName))
            {
                this.searchProductList.push(this.compatibilityMatrix[row][0]);
            }
        }
        this.isSearch= true;
        console.log(this.searchProductList);
    }
}