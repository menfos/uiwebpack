import { Component} from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Http,Response,Headers } from '@angular/http';
import { Product } from '../Product';
import { RequestService} from '../../../shared/request.service';

@Component({
    moduleId: module.id,
    templateUrl: 'productAdd.component.html'
})

export class ProductAddComponent {
    private product: Product;

    constructor(private rs: RequestService, http: Http,private router: Router,private activateRoute: ActivatedRoute ) {       
        this.product = new Product();
        this.product.Country = activateRoute.snapshot.params['country'];
    }

    Done(myItem: Product){
        if(this.IsNull(myItem))
        {
            alert("error");
        }else
            {
                console.log(myItem);
                this.rs.post('api/data/products/create', myItem)
                    .toPromise()
                    .then(response => this.router.navigate(['../../products'], { relativeTo: this.activateRoute }))
                    .catch((error:Error) => {
            });
            }
        
    }
    private IsNull(checkProduct: Product):boolean
    {
        if(checkProduct.ProductId == null || checkProduct.ProductName == null || checkProduct.IsActive == null || checkProduct.Country == null || checkProduct.Cost == null)
        {
            return true;
        }
        
        return false;
    }
}