import { Component } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { ActivatedRoute, Router} from '@angular/router';
import { User } from './User';
import { RequestService} from '../../shared/request.service';

@Component({
    moduleId: module.id,
    templateUrl: 'users.component.html'
})

export class UserComponent {
    private users: User[];
    array: any;

    constructor(private rs: RequestService, http: Http, private router: Router,private activateRoute: ActivatedRoute) {
        this.users = [];
        
        this.rs.get('api/accounts/users')
            .toPromise()
            .then((response: Response) =>{
                let userList = response.json();

                for(let index in userList)
                {
                  let user = userList[index];

                  this.users.push({userName: user.userName, email: user.email, role:user.roles[0], fullName: user.fullName});
                }
              })
            .catch((error:Error) => {
            });       
    }

    Delete(userName: string){
        this.rs.delete('api/accounts/user/'+userName)
            .toPromise()
            .then(response => console.log(response))
            .then(response => this.deleteFromList(userName))
            .catch((error:Error) => {
            });

    }

    private deleteFromList(username:string){
      for(let index in this.users)
      {
        if(this.users[index].userName == username)
        {
          var position = this.users.indexOf(this.users[index]);
          this.users.splice(position,1);
        }
      }
                
    }
}




