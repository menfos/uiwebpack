export class ProductCompatibilityStatus {
    Id: number;
    Country: string;
    ProductId: string;
    CompatibleProductId: string;
    CompatibilityStatus: boolean;
}