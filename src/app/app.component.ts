import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './shared/authentication.service';
import { RequestService } from './shared/request.service';
     
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css']
})
export class AppComponent {
	public isAuth: boolean = false;
    public userName: string = 'No UserName';

    constructor(private reqService:RequestService ,private authService: AuthenticationService, private router: Router) {
        this.isAuth = authService.isAuth;
        this.userName = authService.currentUserName;
        authService.appComponent = reqService.appComponent = this;
    }

    openHomePage() {
        let currentRole = this.authService.currentUserRole;

        if (currentRole !== 'User') {
            this.router.navigate(['admin-home'])
        } else {
            this.router.navigate(['user-home']);
          }
    }

    public signOut() {
        let result = this.authService.signOut();

        if (result) {
            this.isAuth = this.authService.isAuth;
            this.userName = this.authService.currentUserName;
            this.router.navigate(['home']);
        }
    }
}