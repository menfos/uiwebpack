﻿export class User {
    userName: string;
    fullName: string;
    role: string;
    email: string;
}