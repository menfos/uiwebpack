﻿import { Component } from '@angular/core';
import { RequestService } from '../../shared/request.service';
import { AuthenticationService } from '../../shared/authentication.service';
import { Administrator } from './Administrator';

@Component({
    moduleId: module.id,
    templateUrl: 'adminInfo.component.html'
})

export class AdminInfoComponent {
    private admin: Administrator = new Administrator();

    constructor(private rs: RequestService, private authService: AuthenticationService) {
        let currentUserName = authService.currentUserName;

        this.rs.get('api/accounts/user/' + currentUserName)
        	.toPromise()
        	.then((result: any) => {
        		let model = result.json();
        		this.admin.FullName = model.fullName;
        		this.admin.Email = model.email;
        		this.admin.RoleName = model.roles[0];
        		this.admin.UserName = model.userName;
        })
            .catch((error:Error) => {
            });
    }
}






