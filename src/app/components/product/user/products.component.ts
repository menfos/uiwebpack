import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router} from '@angular/router';
import { Product } from '../Product';
import { RequestService} from '../../../shared/request.service';

@Component({
    moduleId: module.id,
    templateUrl: 'products.component.html',
    styleUrls: ['products.component.css']
})
export class ProductComponentUser {
    private products: Product[];
    private countries: string[];
    private country:string;

    constructor(private rs: RequestService, http: Http) {
        this.rs.get('api/countries')
              .toPromise()
              .then(response =>response.json())
              .then((result:any) => this.countries = result)
              .catch((error:Error) => {
            });
    }

    public setCountry(country: string){
      this.country = country;

      this.rs.get('api/data/'+country)
              .toPromise()
              .then(response =>response.json())
              .then((result:any) =>this.products = result.ProductList)
              .catch((error:Error) => {
            });
    }
}
