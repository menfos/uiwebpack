﻿import { Component } from '@angular/core';
import { RequestService} from '../../shared/request.service';
import { AuthenticationService} from '../../shared/authentication.service';
import { User } from './User';

@Component({
    moduleId: module.id,
    templateUrl: 'userInfo.component.html'
})

export class UserInfoComponent {
    public user: User;

    constructor(private rs: RequestService, private authService: AuthenticationService) {
        this.user = new User();
        let currentUserName = authService.currentUserName;
        
        this.rs.get('api/accounts/currentuser')
            .toPromise()
            .then((result: any) => {
                let model = result.json();
                this.user.fullName = model.fullName;
                this.user.email = model.email;
                this.user.role = model.roles[0];
                this.user.userName = model.userName;
            })   
            .catch((error:Error) => {
            });
    }
}