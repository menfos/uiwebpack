﻿export class Product {
    Id: number;
    ProductName: string;
    ProductId: string;
    Cost: number;
    Country: string;
    IsActive: boolean;
}