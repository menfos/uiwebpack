import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RequestService } from './request.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthenticationService {
    public currentUserRole: string = 'No Role';
    public currentUserName: string = 'No UserName';
    public currentToken:string = 'No Token';
    public currentExpiresAt: number = 0;
    public isAuth: boolean = false;
    public appComponent: any = null;

    constructor(private rs: RequestService,private http: Http) {
        this.currentUserName = localStorage.getItem('userName');
        this.currentUserRole = localStorage.getItem('userRole');
        
        if(this.currentUserRole != null && this.currentUserRole.length > 0)
        {
            this.isAuth = true;
        }else{
                this.isAuth = false;
             }
    }

    async login(username: string, password: string) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var params = new URLSearchParams();
        params.set('username', username);
        params.set('password', password);
        params.set('grant_type', "password");

        let isLoged = await this.http.post(this.rs._baseUrl+'oauth/token', params.toString(), {headers : headers})
            .toPromise()
            .then((response: any) => {
                let body = response.json();
                this.currentToken = body.access_token;
                this.currentUserName = username;
                this.currentExpiresAt = new Date().getTime() + (body.expires_in*1000);
                return body;
            })
            .catch((error: any) => {
                alert("Uncorrect data");
            })

        if(isLoged)
        {
            let model = await this.getUserRoles();
            this.currentUserRole = model[0];
            this.isAuth = true;
            this.appComponent.isAuth = true;
            this.appComponent.userName = this.currentUserName;

            localStorage.setItem('token', this.currentToken);
            localStorage.setItem('userName', this.currentUserName);
            localStorage.setItem('userRole', this.currentUserRole);
            localStorage.setItem('expires_at', this.currentExpiresAt.toString());

            return isLoged;
        }
    }

    async getUserRoles(): Promise<string[]>{

        let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + this.currentToken});

        const response = await this.http.get(this.rs._baseUrl+'api/accounts/roles', {headers: headers})
            .toPromise();

        return response.json();
    }

    signOut() {
        this.isAuth = false;
        this.currentUserName = 'No UserName';
        this.currentUserRole = 'No Role';
        this.currentToken = 'No Token';
        this.currentExpiresAt = 0;

        localStorage.removeItem('token');
        localStorage.removeItem('userName');
        localStorage.removeItem('userRole');
        localStorage.removeItem('expires_at');

        return true;
    }
}