import { Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http,Response,Headers } from '@angular/http';
import { RequestService } from '../../shared/request.service';

@Component({
    moduleId: module.id,
    templateUrl: 'userChangePassword.component.html'
})
export class UserChangePasswordComponent {
    private changePasswordModel: any;
    private token: string;

    constructor(private rs: RequestService, http: Http, private router: Router,private activateRoute: ActivatedRoute) {
        this.token = localStorage.getItem('token');
        this.changePasswordModel = {
            "OldPassword" : '',
            "NewPassword" :'',
            "ConfirmPassword" :''
        };       
    }

    Change(myItem: any){
        if(myItem.NewPassword == myItem.ConfirmPassword){
            this.rs.post('api/accounts/ChangePassword', myItem)
                .toPromise()
                .then(response => this.router.navigate(['../info'], { relativeTo: this.activateRoute }))
                .catch((error:Error) => {
                });  
        }else{
            alert("Passwords don't match.");
        }      
    }

}