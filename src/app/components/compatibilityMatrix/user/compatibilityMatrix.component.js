"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var Bundle_1 = require("../../../../app/components/bundle/Bundle");
var request_service_1 = require("../../../shared/request.service");
require("rxjs/Rx");
var CompatibilityMatrixComponentUser = (function () {
    function CompatibilityMatrixComponentUser(http, rs, router, activateRoute) {
        var _this = this;
        this.rs = rs;
        this.router = router;
        this.activateRoute = activateRoute;
        this.bundle = new Bundle_1.Bundle();
        this.changeList = this.searchProductList = [];
        this.country = this.searchText = '';
        this.isSearch = false;
        this.rs.get('api/countries')
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (result) { return _this.countries = result; })
            .catch(function (error) {
        });
    }
    CompatibilityMatrixComponentUser.prototype.setCountry = function (country) {
        this.isSearch = false;
        this.searchText = '';
        this.country = country;
        this.refreshMatrix();
    };
    CompatibilityMatrixComponentUser.prototype.refreshMatrix = function () {
        var _this = this;
        this.isSearch = false;
        this.searchText = '';
        this.rs.get('api/data/' + this.country)
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (result) { return _this.bundle = result; })
            .then(function (response) { return _this.initializeCompatibilityMatrix(_this.bundle.ProductList); })
            .catch(function (error) {
        });
    };
    CompatibilityMatrixComponentUser.prototype.initializeCompatibilityMatrix = function (productsList) {
        this.compatibilityMatrix = [];
        for (var row = 0; row <= productsList.length; row++) {
            this.compatibilityMatrix[row] = [];
        }
        this.compatibilityMatrix[0][0] = this.country;
        for (var row = 1; row <= productsList.length; row++) {
            this.compatibilityMatrix[0][row] = productsList[row - 1].ProductId;
            this.compatibilityMatrix[row][0] = productsList[row - 1].ProductId;
        }
        for (var row = 1; row <= productsList.length; row++) {
            for (var column = 1; column <= productsList.length; column++) {
                this.compatibilityMatrix[row][column] = this.getCompatibleStatus(this.compatibilityMatrix[row][0], this.compatibilityMatrix[0][column]);
            }
        }
    };
    CompatibilityMatrixComponentUser.prototype.getCompatibleStatus = function (product1, product2) {
        for (var index = 0; index < this.bundle.CompatibilityList.length; index++) {
            if (this.bundle.CompatibilityList[index].ProductId == product1 &&
                this.bundle.CompatibilityList[index].CompatibleProductId == product2) {
                if (this.bundle.CompatibilityList[index].CompatibilityStatus) {
                    return "1";
                }
                else {
                    return "0";
                }
            }
        }
        return "";
    };
    CompatibilityMatrixComponentUser.prototype.Search = function (productName) {
        this.searchProductList = [];
        var count = 1;
        for (var row = 1; row < this.compatibilityMatrix[0].length; row++) {
            if (this.compatibilityMatrix[row][0].includes(productName)) {
                this.searchProductList.push(this.compatibilityMatrix[row][0]);
            }
        }
        this.isSearch = true;
        console.log(this.searchProductList);
    };
    return CompatibilityMatrixComponentUser;
}());
CompatibilityMatrixComponentUser = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'compatibilityMatrix.component.html',
        styleUrls: ['compatibilityMatrix.component.css']
    }),
    __metadata("design:paramtypes", [http_1.Http, request_service_1.RequestService, router_1.Router, router_1.ActivatedRoute])
], CompatibilityMatrixComponentUser);
exports.CompatibilityMatrixComponentUser = CompatibilityMatrixComponentUser;
//# sourceMappingURL=compatibilityMatrix.component.js.map